<?php

//Тестовое задание. Дмитрий Никульшин (Skype: webenjoy, CV: https://hh.ru/resume/ae554182ff05d0cd520039ed1f523545677769)
//Запуск: конфиг базы находится в методе dbConnect класса Parser. После настройки подключения нужно выполнить миграцию - зайти на страницу,
//нажать "Do a migration", после чего можно переходить к другим действиям (отобразить информацию из фида или сохранить её в базу).

//Примечание: В некоторых частях отсутствует валидация (тип данных, наличие и др.), отсутствует оптимизация запросов (запросы в цикле, нет транзакций).
//Это вопрос экономии времени, а не отсутствия знаний.
//Сознательно не приводил структуру БД в порядок по той же причине (это тестовое задание, не продакшн). Базовая нормализация присутствует.

namespace Test;

use Test\Parser;

require "src/Parser.php";

if (isset($_GET["action"])) {
    switch ($_GET["action"]) {
        case "migrate":
            Parser::dbConnect()
                  ->migrate();
            echo "Reload the page";
            break;
        case "save":
            Parser::getResult()
                  ->parseXml()
                  ->saveData();
            break;
    }
}

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>XML Feed Parser - Test task</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <?php if (!Parser::checkDB()) { ?><a href="?action=migrate">Do a migration</a><?php } ?>
        <?php if (Parser::checkDB()) { ?><a href="?action=display">Display feed</a><?php } ?>
        <?php if (Parser::checkDB()) { ?><a href="?action=save">Save feed</a><?php } ?>

        <?php if (isset($_GET["action"]) && $_GET["action"] == "display") {

                $data = Parser::getResult()
                              ->parseXml()
                              ->out();

                if ($data) {

                    echo '<div class="container">';

                    foreach ($data["properties"] as $name => $property) {
                        ?>
                        <br />
                        <?php echo "ЖК " . $name; ?><br />
                        Минимальная стоимость: <?php echo number_format($property["minPrice"], 0, ".", " "); ?> RUR<br />
                        Максимальная стоимость: <?php echo number_format($property["maxPrice"], 0, ".", " "); ?> RUR<br />
                        Количество корпусов ЖК (building-section): <?php echo sizeof($property["sections"]); ?><br />
                        Количество квартир в ЖК: <?php echo sizeof($property["flats"]); ?><br />
                        Дата последнего обновления: <?php echo date("Y-m-d H:i", strtotime($property["last_update"])); ?><br />
                        <img style="width: 250px;" src="<?php echo $property["plan"]; ?>" alt="">
                        <br />
                        <?php
                    }

                    foreach ($data["items"] as $id => $item) {

                            ?>

                            <div class="block">
                                <div class="img">
                                    <a href="<?php echo $item["image"] ?>" target="_blank">
                                        <img src="<?php echo $item["image"] ?>" alt="">
                                    </a>
                                </div>
                                <div class="base-data">
                                    <a href="<?php echo $item["url"] ?>" target="_blank">
                                        <?php echo $item["location"]["address"] ?><br /><br />
                                    </a>
                                    ID: <?php echo $id ?><br />
                                    Тип: <?php echo $item["type"] ?><br />
                                    Тип собственности: <?php echo $item["property-type"] ?><br />
                                    Категория: <?php echo $item["category"] ?><br />
                                    Статус сделки: <?php echo $item["deal-status"] ?><br />
                                    Дата создания: <?php echo date("Y-m-d H:i", strtotime($item["creation-date"])) ?><br />
                                    Дата обновления: <?php echo date("Y-m-d H:i", strtotime($item["last-update-date"])) ?>
                                </div>
                                <div class="base-data">
                                    Страна: <?php echo $item["location"]["country"] ?><br />
                                    Населенный пункт: <?php echo $item["location"]["locality-name"] ?><br />
                                    Район: <?php echo $item["location"]["sub-locality-name"] ?><br />
                                    Адрес: <?php echo $item["location"]["address"] ?><br />
                                    Длина: <?php echo $item["location"]["longitude"] ?><br />
                                    Широта: <?php echo $item["location"]["latitude"] ?><br />
                                    Метро: <?php echo $item["location"]["metro"]["name"] ?><br />
                                </div>
                                <div class="base-data">
                                    Ипотека: <?php echo $item["mortgage"] ? "да" : "нет" ?><br />
                                    Стоимость: <?php echo number_format($item["price"]["value"], 0, ".", " ") . " "; echo $item["price"]["currency"] ?><br />
                                    Площадь: <?php echo $item["area"]["value"] . " "; echo $item["area"]["unit"] ?><br />
                                    Жилая площадь: <?php echo $item["living-space"]["value"] . " "; echo $item["living-space"]["unit"] ?><br />
                                    Ремонт: <?php echo $item["renovation"] ?><br />
                                    Новая: <?php echo $item["new-flat"] ?><br />
                                    Кол-во комнат: <?php echo $item["rooms"] ?><br />
                                    Студия: <?php echo isset($item["studio"]) ? $item["studio"] : 'нет' ?><br />
                                    Этаж: <?php echo $item["floor"] ?><br />
                                    <?php echo isset($item["building-section"]) ? "Корпус: " . $item["building-section"] : "" ?>
                                </div>
                                <div class="base-data">
                                    Кол-во этажей: <?php echo $item["floors-total"] ?><br />
                                    Название ЖК: <?php echo $item["building-name"] ?><br />
                                    Год постройки (сдачи): <?php echo $item["built-year"] ?><br />
                                    Квартал сдачи: <?php echo $item["ready-quarter"] ?><br />
                                    Статус постройки: <?php echo $item["building-state"] ?><br />
                                    Очередь строительства: <?php echo $item["building-phase"] ?><br />
                                    Тип дома: <?php echo $item["building-type"] ?><br />
                                    ID Yandex: <?php echo sizeof($item["yandex-building-id"]) ? $item["yandex-building-id"] : "" ?>
                                </div>
                                <div class="base-data">
                                    Телефон агента: <?php echo $item["sales-agent"]["phone"] ?><br />
                                    Организация: <?php echo $item["sales-agent"]["organization"] ?><br />
                                    Сайт: <a href="<?php echo $item["sales-agent"]["url"] ?>" target="_blank"><?php echo $item["sales-agent"]["url"] ?></a><br />
                                    Тип: <?php echo $item["sales-agent"]["category"] ?><br />
                                    <img src="<?php echo $item["sales-agent"]["photo"] ?>" style="width: 75px;" alt="">
                                </div>
                            </div>

                            <?php
                        // }
                    }

                    echo '</div>';
                }

            }
        ?>


        </div>

    </body>
</html>
