<?php

namespace Test;

class Parser {

    public static $data;
    public static $parsedData;
    private static $connection;
    private static $_instance;

    public function __destruct() {
        if (self::$connection) {
            pg_close(self::$connection);
        }
    }

    public function setInstance() {
        if (!self::$_instance) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    public function getResult($target = "http://www.spbren.ru/upload/yandexFeedKurort.xml") {
        if ($target) {

            $instance = self::setInstance();
            $ch = curl_init($target);

            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $data = curl_exec($ch);
                    curl_close($ch);

            if ($data) {
                self::$data = $data;
            } else {
                die("No input data found");
            }

        }

        return $instance;
    }

    public function parseXml() {
        if (self::$data) {
            $parsed = [];

            $xml = new \SimpleXMLElement(self::$data);
            $dt = json_decode(json_encode($xml), true);

            foreach ($dt["offer"] as $offer) {
                $parsed[$offer["@attributes"]["internal-id"]] = $offer;
                unset($parsed[$offer["@attributes"]["internal-id"]]["@attributes"]);
            }

            self::$parsedData = $parsed;
        }

        return $this;
    }

    public function dbConnect() {
        $instance = self::setInstance();
        $connectionStr = "host=localhost dbname=test user=test password=q1w2e3";

        if ($cn = pg_connect($connectionStr)) {
            self::$connection = $cn;
        } else {
            self::$connection = false;
            die("Failed establishing connection");
        }

        return $instance;
    }

    public function checkDB() {
        self::dbConnect();

        $test = @pg_query(self::$connection, "SELECT id FROM items LIMIT 1");

        if ($test) {
            return true;
        }

        return false;

    }

    public function migrate() {
        if (self::$connection) {
            $test = pg_query(self::$connection, "SELECT * FROM items");

            if (!$test) {

                try {

                    pg_query(self::$connection, "CREATE TABLE items (
                        id int primary key NOT NULL,
                        type varchar NOT NULL,
                        property_type varchar NOT NULL,
                        category varchar NOT NULL,
                        url varchar,
                        description text,
                        creation_date timestamp NOT NULL,
                        last_update_date timestamp NOT NULL,
                        expire_date timestamp,
                        mortgage varchar,
                        deal_status varchar,
                        image varchar NOT NULL
                    )");

                    pg_query(self::$connection, "CREATE TABLE properties (
                        item_id int primary key NOT NULL,
                        price int NOT NULL,
                        price_currency varchar NOT NULL,
                        price_unit varchar,
                        area int,
                        area_unit varchar,
                        living_space varchar,
                        room_space float,
                        room_space_unit varchar,
                        kitchen_space float,
                        kitchen_space_unit varchar,
                        renovation varchar,
                        new_flat varchar NOT NULL,
                        rooms int NOT NULL,
                        rooms_type varchar,
                        studio varchar,
                        apartments varchar,
                        yandex_house_id varchar,
                        yandex_building_id varchar,
                        building_name varchar,
                        building_state varchar NOT NULL,
                        building_phase varchar,
                        building_type varchar,
                        building_series varchar,
                        building_section varchar,
                        ceiling_height float,
                        bathroom_unit varchar,
                        floor_covering varchar,
                        window_view varchar,
                        open_plan varchar,
                        balcony varchar,
                        lift varchar,
                        rubbish_chute varchar,
                        guarded_building varchar,
                        is_elite varchar,
                        parking varchar,
                        built_year int,
                        ready_quarter int,
                        floor int NOT NULL,
                        floors_total int
                    )");

                    pg_query(self::$connection, "CREATE TABLE sales_agent (
                        item_id int primary key NOT NULL,
                        name varchar,
                        phone varchar NOT NULL,
                        category varchar NOT NULL,
                        organization varchar,
                        url varchar,
                        email varchar,
                        photo varchar
                    )");

                    pg_query(self::$connection, "CREATE TABLE location (
                        item_id int primary key NOT NULL,
                        country varchar NOT NULL,
                        region varchar,
                        district varchar,
                        locality_name varchar,
                        sub_locality_name varchar,
                        metro json,
                        railway_station varchar,
                        address varchar,
                        direction varchar,
                        distance varchar,
                        latitude varchar,
                        longitude varchar
                    )");


                } catch (\Exception $e) {
                    die($e->getMessage());
                }

            }

        }
    }

    public function out() {
        if (self::$parsedData) {

            $properties = [];

            foreach (self::$parsedData as $key => &$item) {
                if (!isset($properties[$item["building-name"]])) {
                    $properties[$item["building-name"]] = [
                        "prices" => [],
                        "sections" => [],
                        "flats" => [],
                        "plan" => $item["image"],
                        "last_update" => $item["last-update-date"]
                    ];
                }
            }

            foreach (self::$parsedData as $item) {
                if ($item["price"]["currency"] == "RUR") {
                    array_push($properties[$item["building-name"]]["prices"], $item["price"]["value"]);
                }

                if (isset($item["building-section"]) && !isset($properties[$item["building-name"]]["sections"][$item["building-section"]])) {
                    array_push($properties[$item["building-name"]]["sections"], $item["building-section"]);
                }

                if (isset($item["building-name"])) {
                    array_push($properties[$item["building-name"]]["flats"], $key);
                }
            }

            foreach ($properties as $key => $item) {
                $properties[$key]["minPrice"] = min($properties[$key]["prices"]);
                $properties[$key]["maxPrice"] = max($properties[$key]["prices"]);
                unset($properties[$key]["prices"]);
            }

            $params["items"] = self::$parsedData;
            $params["properties"] = $properties;

            return $params;
        } else {
            return false;
        }
    }

    public function saveData() {

        self::dbConnect();

        if (!self::$connection) {
            die("No DB connection established!");
        }

        if (!self::checkDB()) {
            die("No migration launched");
        }

        if (self::$parsedData) {

            $dataMap = [
                "item" => [
                    "internal_id" => false,
                    "type" => false,
                    "property_type" => false,
                    "category" => false,
                    "url" => false,
                    "description" => false,
                    "creation_date" => false,
                    "last_update_date" => false,
                    "expire_date" => false,
                    "mortgage" => false,
                    "deal_status" => false,
                    "image" => false,
                    "location_id" => false,
                    "sales_agent_id" => false,
                    "properties_id" => false
                ],

                "properties" => [
                    "item_id" => false,
                    "price" => false,
                    "price_currency" => false,
                    "price_unit" => false,
                    "area" => false,
                    "area_unit" => false,
                    "living_space" => false,
                    "room_space" => false,
                    "kitchen_space" => false,
                    "renovation" => false,
                    "new_flat" => false,
                    "rooms" => false,
                    "rooms_type" => false,
                    "studio" => false,
                    "apartments" => false,
                    "yandex_house_id" => false,
                    "yandex_building_id" => false,
                    "building_name" => false,
                    "building_state" => false,
                    "building_phase" => false,
                    "building_type" => false,
                    "building_series" => false,
                    "building_section" => false,
                    "ceiling_height" => false,
                    "bathroom_unit" => false,
                    "floor_covering" => false,
                    "window_view" => false,
                    "open_plan" => false,
                    "balcony" => false,
                    "lift" => false,
                    "rubbish_chute" => false,
                    "guarded_building" => false,
                    "is_elite" => false,
                    "parking" => false,
                    "built_year" => false,
                    "ready_quarter" => false,
                    "floor" => false,
                    "floors_total" => false
                ]

            ];

            $source = self::$parsedData;

            foreach ($source as $internal_id => &$item) {

                $cache = [
                    "item" => [],
                    "properties" => [],
                    "location" => [],
                    "sales_agent" => []
                ];

                $cache["item"]["id"] = $internal_id;

                foreach ($item as $key => $data) {

                    $k = str_replace("-", "_", $key);

                    if (isset($dataMap["item"][$k])) {
                        if (gettype($data) === "array") {
                            $cache["item"][$k] = json_encode($data);
                        } else {
                            $cache["item"][$k] = (String)$data;
                        }
                    }

                    if (isset($dataMap["properties"][$k])) {
                        switch ($k) {
                            case "price":
                                $cache["properties"]["price"] = (int)$data["value"];
                                $cache["properties"]["price_currency"] = $data["currency"];
                                break;
                            case "area":
                                $cache["properties"]["area"] = (float)$data["value"];
                                $cache["properties"]["area_unit"] = $data["unit"];
                                break;
                            case "living_space":
                                $cache["properties"]["living_space"] = (float)($data["value"]);
                                break;
                            case "room_space":
                                $cache["properties"]["room_space"] = (float)($data["value"]);
                                $cache["properties"]["room_space_unit"] = (float)($data["unit"]);
                                break;
                            case "kitchen_space":
                                $cache["properties"]["kitchen_space"] = (float)($data["value"]);
                                $cache["properties"]["kitchen_space_unit"] = (float)($data["unit"]);
                                break;
                            default:
                                if (gettype($data) === "array" && sizeof($data) == 0) {
                                    $data = NULL;
                                }

                                $cache["properties"][$k] = (String)$data;
                                break;
                        }

                    }

                    if ($k == "location") {
                        $outLocation = [];

                        foreach ($data as $entryKey => $entry) {
                            $replacedKey = str_replace("-", "_", $entryKey);

                            if (gettype($entry) === "array") {
                                $entryValue = json_encode($entry);
                            } else {
                                $entryValue = (String)$entry;
                            }

                            $outLocation[$replacedKey] = $entryValue;
                        }

                        $cache["location"] = $outLocation;
                    }

                    if ($k == "sales_agent") {
                        $outSalesAgent = [];

                        foreach ($data as $entryKey => $entry) {
                            $replacedKey = str_replace("-", "_", $entryKey);

                            if (gettype($entry) === "array") {
                                $entryValue = json_encode($entry);
                            } else {
                                $entryValue = (String)$entry;
                            }

                            $outSalesAgent[$replacedKey] = $entryValue;
                        }

                        $cache["sales_agent"] = $outSalesAgent;
                    }

                }

                $cache["location"]["item_id"] = $internal_id;
                $cache["sales_agent"]["item_id"] = $internal_id;
                $cache["properties"]["item_id"] = $internal_id;

                if (sizeof($cache["item"]) > 0) {

                    if (pg_affected_rows(pg_query("SELECT id FROM items WHERE id = " . $cache["item"]["id"])) > 0) {
                        if (!pg_update(self::$connection, "items", $cache["item"], ["id" => $cache["item"]["id"]])) {
                            print_r($cache["item"]);
                            die("Error updating data => items");
                        }
                    } else {
                        if (!pg_insert(self::$connection, "items", $cache["item"])) {
                            print_r($cache["item"]);
                            die("Error inserting data => items");
                        }
                    }

                    if (pg_affected_rows(pg_query("SELECT item_id FROM location WHERE item_id = " . $cache["item"]["id"])) > 0) {
                        if (!pg_update(self::$connection, "location", $cache["location"], ["item_id" => $cache["item"]["id"]])) {
                            print_r($cache["location"]);
                            die("Error updating data => location");
                        }
                    } else {
                        if (!pg_insert(self::$connection, "location", $cache["location"])) {
                            print_r($cache["location"]);
                            die("Error inserting data => location");
                        }
                    }

                    if (pg_affected_rows(pg_query("SELECT item_id FROM sales_agent WHERE item_id = " . $cache["item"]["id"])) > 0) {
                        if (!pg_update(self::$connection, "sales_agent", $cache["sales_agent"], ["item_id" => $cache["item"]["id"]])) {
                            print_r($cache["sales_agent"]);
                            die("Error updating data => sales_agent");
                        }
                    } else {
                        if (!pg_insert(self::$connection, "sales_agent", $cache["sales_agent"])) {
                            print_r($cache["sales_agent"]);
                            die("Error inserting data => sales_agent");
                        }
                    }

                    if (pg_affected_rows(pg_query("SELECT item_id FROM properties WHERE item_id = " . $cache["item"]["id"])) > 0) {
                        if (!pg_update(self::$connection, "properties", $cache["properties"], ["item_id" => $cache["item"]["id"]])) {
                            print_r($cache["properties"]);
                            die("Error updating data => items");
                        }
                    } else {
                        if (!pg_insert(self::$connection, "properties", $cache["properties"])) {
                            print_r($cache["properties"]);
                            die("Error inserting data => items");
                        }
                    }

                }

            }

            echo "Saving complete<br />";

        }

        return $this;
    }

    public function displayRaw() {
        if (self::$data) {
            header('Content-Type: application/xml; charset=utf-8');
            print_r(self::$data);
        }

        return true;
    }

    public function displayJSON() {
        if (self::$parsedData) {
            header('Content-Type: application/json');
            echo json_encode(self::$parsedData);
        }

        return true;
    }


}
